function addTokens(input, tokens){
        if (typeof(input) !== 'string') {
            throw Error('Invalid input') 
        } else if (input.length < 6) {
            throw Error('Input should have at least 6 characters')
        }
        tokens.forEach(element => {
            let key = 'tokenName'
            if (!(element.hasOwnProperty(key) && typeof element[key] === 'string')) {
                throw Error('Invalid array format')
            }
        })
        // if (input.indexOf('...') === -1) {
        //     return input
        // } else {
            tokens.forEach(element => {
                input = input.replace('...','${' + `${element['tokenName']}` + '}')
            })
            return input
        // }
}

const app = {
    addTokens: addTokens
}

module.exports = app;